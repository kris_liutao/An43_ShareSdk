package com.lt.an43_sharesdk;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.sina.weibo.SinaWeibo;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ShareSDK.initSDK(this);//初始化ShareSDK
    }
    /*
        点击按钮将数据分享到新浪微博平台
         */
    public void sharedSina(View view) {
        //1.获取当前分享的平台实例
        Platform platform=ShareSDK.getPlatform(SinaWeibo.NAME);
        //2.添加相应的监听事件
        platform.setPlatformActionListener(new PlatformActionListener() {
            //表示分享开始执行时回调的函数
            @Override
            public void onComplete(Platform platform, int action, HashMap<String, Object> hashMap) {
                if(action==Platform.ACTION_AUTHORIZING){
                    Log.i("tag","授权成功!");
                    //构建分享对象并且启动分享
                    SinaWeibo.ShareParams params=new SinaWeibo.ShareParams();
                    params.setText("快来下载Kris的APP啦....");
                    //启动分享
                    platform.share(params);
                }else if(action==Platform.ACTION_SHARE){
                    Log.i("tag","分享成功!");
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                Log.i("tag","分享出错");
                throwable.printStackTrace();
            }

            @Override
            public void onCancel(Platform platform, int i) {
                Log.i("tag","取消分享");
            }
        });
        //3.授权
        platform.authorize();
    }

    /**
     * 一键分享
        */
public void sharedOneKey(View view){
        //1.创建一键分享对象
        OnekeyShare share=new OnekeyShare();
        share.disableSSOWhenAuthorize();//关闭sso授权
        //设置一键分享的内容
        share.setText("Kris未签名上线APP能实现一键分享了....");
        share.show(this);
        }
        }

